import 'dart:html';
import 'package:pmap/pmap.dart';
import 'package:polymer/polymer.dart';

@CustomTag('note-add')
class CategoryAdd extends PolymerElement {
  @published Notes notes;

  CategoryAdd.created() : super.created();

  add(Event e, var detail, Node target) {
    InputElement code = $['code'];     // new DateTime.now();

      var note = new Note();
      note.code = new DateTime.now().toString();
      if (notes.add(note)) {
        notes.order();
    }
  }
}