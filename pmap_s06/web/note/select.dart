import 'dart:html';
import 'package:pmap/pmap.dart';
import 'package:polymer/polymer.dart';

@CustomTag('note-select')
class CategoryTable extends PolymerElement {
  @published Notes notes;
  Note note;
  @observable bool showAdd = true;
  @observable bool showEdit = false;

  CategoryTable.created() : super.created();


  add (Event e, var detail, Element target) {
    String code = target.attributes['note-code'];
    note = notes.find(code);
    showEdit = true;
  }

  edit(Event e, var detail, Element target) {
    String code = target.attributes['note-code'];
    note = notes.find(code);
    showEdit = true;
  }

  delete(Event e, var detail, Element target) {
    String code = target.attributes['note-code'];
    note = notes.find(code);
    notes.remove(note);
  }

}