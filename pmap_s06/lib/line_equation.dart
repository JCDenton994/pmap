part of pmap;

class LineEquation {

// y = ax + b.
num a = 0, b = 0, x = 0, y = 0;
bool horizontal = false, vertical = false;

LineEquation (Point p1, Point p2) {
  num deltaX = p2.x - p1.x;
  num deltaY = p2.y - p1.y;
  // Reverse y axis.
  //deltaY = deltaY * -1;
  
  if (deltaX == 0) {
    // vertical line.
    vertical = true;
    x = p1.x;
  } else if (deltaY == 0) {
    // horizontal line.
    horizontal = true;
    y = p1.y;
  } else {
    // y - y1 = a(x - x1)
    a = deltaY/deltaX;
    num y1 = p1.y;
    num x1 = p1.x;
    // Reverse slope.
    //b = (x1 * a) + y1;
    b = (x1 * -a) + y1;
  }
}

num getX(num y) {
  if (vertical) {
    return x;
  } else if (horizontal) {
    return (this.y == y) ? double.INFINITY : null;
  } else {
    // y = ax + b.
    // x = (y - b)/a
    return (y - b)/a;
  }
}

getY(num x) {
  if (vertical) {
    return (this.x == x) ? double.INFINITY : null;
  } else if (horizontal) {
    return y;
  } else {
    // y = ax + b.
    return a * x + b;
  }
}

}
    