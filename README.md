# ![Alt pmap] (https://bitbucket.org/JCDenton994/pmap/raw/e6297e6c4da6d51a15c7d799e6ab5b19a30f9ad2/pmap_s07/web/img/Sans%20titre.png)

Projet PMAP
SIO-6014 
Alexandre April (905 165 501)
David M. Lambert (908 140 097)
Rémi Talbot (907 256 019)

PMAP is a simple application used for process mapping. The kernel is based on model_concepts (see : https://github.com/dzenanr/model_concepts)
Refer to the user manual in the doc repository of the last spiral for more information.