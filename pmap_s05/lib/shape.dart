part of pmap;

class Shape {
  final Board board;
  String name;
  num id;
  num x;
  num y;
  String type;
  
  ImageElement image;
  num width = 0;
  num height = 0;
  
  bool data_object = false;
  bool data_store = false;
  bool end = false;
  bool event = false;
  bool gateway = false;
  bool message = false;
  bool start = false;
  bool sub = false;
  bool task = false;
  bool entry = false;
  bool _selected = false;
  bool _hidden = false;
  bool _mouseDown = false;
  
bool contains(int pointX, int pointY) {
  if((pointX > x) && (pointY > y)) {
      return true;
  } else {
      return false;
  }
}
Shape(this.board, this.id, this.name, this.x, this.y, this.type) {
  draw(this.x, this.y, this.type);
  document.querySelector('#canvas').onMouseDown.listen(onMouseDown);
  document.querySelector('#canvas').onMouseUp.listen(onMouseUp);  
  document.querySelector('#canvas').onMouseMove.listen(onMouseMove);
  select();
}

void draw(x, y, type) {
    image = new ImageElement(src: "img/"+type+".png");
    image.onLoad.listen((e) {
      board.context.drawImage(image, x, y);
      height = image.height;
      width = image.width;
    });
    if (isSelected()) {
      board.context.strokeStyle = Board.SELECTION_COLOR;
      board.context.lineWidth = Board.SELECTION_WIDTH;
      board.context.strokeRect(x, y, 102, 40);
    }
    board.context.strokeStyle = Board.DEFAULT_LINE_COLOR;
    board.context.strokeRect(x, y, 102, 40);
    
    //drawLinkedFlow();
}

void drawLinkedFlow() {
  Flow flow = board.getFlowStartingFromShape(this);
  if (flow != null) {
    flow.draw();
  }
}

void select() {
  _selected = true;
  if (board.lastShapeSelected != this) {
    board.beforeLastShapeSelected = board.lastShapeSelected;
  }
  board.lastShapeSelected = this;
  board.toolBar.bringSelectedShape();
}

void deselect() {
  _selected = false;
  if (board.lastShapeSelected == this){
    board.lastShapeSelected = board.beforeLastShapeSelected;
    board.beforeLastShapeSelected = null;
  } else if (board.beforeLastShapeSelected == this) {
    board.beforeLastShapeSelected = null;
  }
}

bool isSelected() => _selected;

void toggleSelection() {
  if (isSelected()) {
    deselect();
  } else {
    select();
  }
}

Map<String, Object> toJson(){
  Map<String, Object> shapeMap = new Map<String, Object>();
  shapeMap["id"] = id;
  shapeMap["name"] = name;
  shapeMap["x"] = x;
  shapeMap["y"] = y;
  shapeMap["height"] =  height;
  shapeMap["width"] = width;
  shapeMap["type"] = type;
  return shapeMap;
}

String toString() => '$id, $name, $type ($x, $y)';

void onMouseDown(MouseEvent e) {
  _mouseDown = true;
  if (board.toolBar.isSelectToolOn() && contains(e.offset.x, e.offset.y)) {
    toggleSelection();
  }
}

void onMouseUp(MouseEvent e) {
  _mouseDown = false;
}


void onMouseMove(MouseEvent e) {
  // RT: j'ai mis le code en commentaire pcq ça générait des problèmes
  /*if (contains(e.offset.x, e.offset.y) && _mouseDown &&
    board.countShapesContain(e.offset.x, e.offset.y) < 2) {
     x = (e.offset.x / 2).toInt();
     if (x < 0) {
    x = 1;
     }
     if (x > board.width) {
    x = board.width - 1;
     }
     y = (e.offset.y / 2).toInt();
     if (y < 0) {
    y = 1;
     }
     if (y > board.height) {
    y = board.height - 1;
     }
  }   */
}

Point center() {
  int centerX = (x + width / 2).toInt();
  int centerY = (y + height / 2).toInt();
  return new Point(centerX, centerY);
}

/**
 * Return the intersection point of the line between the begin <x1,y1>
 * and end <x2,y2> points with this shape;
 * <x1,y1> is inside the shape, <x2,y2> may be inside or outside.
 * Fast algorithm.
 */
Point getIntersectionPoint(Point lineBeginPoint, Point lineEndPoint) {
  num x1 = lineBeginPoint.x;
  num y1 = lineBeginPoint.y;
  num x2 = lineEndPoint.x;
  num y2 = lineEndPoint.y;
  if (x2 == x1) { /* vertical line */
    return new Point(x2, (y2 < y1 ? y : y + height));
  }
  if (y2 == y1) { /* horizontal line */
    return new Point((x2 < x1 ? x : x + width), y2);
  }

  num m = (y2 - y1) / (x2 - x1);
  num xx = (x2 < x1 ? x : x + width);
  num fy = m * (xx - x2) + y2;
  num yy;
  /* float comparison, because fy may be bigger than the biggest integer */
  if (fy >= y && fy <= y + height) {
    //yy = fy.toInt();
    yy = fy;
  } else {
    yy = (y2 < y1 ? y : y + height);
    xx = (fy - y2) ~/ m + x2;
  }
  return new Point(xx, yy);
}

}