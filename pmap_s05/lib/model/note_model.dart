part of note;

class NoteModel extends ConceptModel {
  static final String note = 'Note';

  Map<String, ConceptEntities> newEntries() {
    var notes = new Notes();
    var map = new Map<String, ConceptEntities>();
    map[note] = notes;
    return map;
  }

  Notes get notes => getEntry(note);

  init() {
    var webNote = new Note();
    webNote.code = new DateTime.now().toString();
    webNote.user = 'alapr5';
    webNote.comment = 'test de note init';
    notes.add(webNote);

    notes.add(webNote);
    notes.order();

  }
}
