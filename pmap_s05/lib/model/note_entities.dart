part of note;

class Note extends ConceptEntity<Note> {
  String user, comment;

  Note newEntity() => new Note();

  String toString() {
    return '  {\n '
           '    ${super.toString()}, \n '
           '    user: ${user}\n'
           '    comment: ${comment}\n'
           '  }\n';
  }

  Map<String, Object> toJson() {
    Map<String, Object> entityMap = super.toJson();
    entityMap['user'] = user;
    entityMap['comment'] = comment;

    return entityMap;
  }

  fromJson(Map<String, Object> entityMap) {
    super.fromJson(entityMap);
    user = entityMap['user'];
    comment = entityMap['comment'];
  }
}

class Notes extends ConceptEntities<Note> {
  Notes newEntities() => new Notes();
  Note newEntity() => new Note();
}
