part of pmap;

class Flow {

  final Board board;
  Shape input;
  Shape output;
  String id; // RT: attribut non utilisé 
  String name; // RT: attribut non utilisé 
  
  bool _selected = false;

Flow (this.board, this.input, this.output) {
  //id = ' '; //add increment to this attribute
  name = ' '; //will be updated by toolbar
  draw();
  select();
}

void draw() {
  board.context.beginPath();
  board.context.moveTo(input.center().x, input.center().y);
  board.context.lineTo(output.center().x, output.center().y);
  
  board.context.strokeStyle = Board.SELECTION_COLOR;
  
  if (isSelected()) {
    board.context.lineWidth = Board.DEFAULT_LINE_WIDTH + 2;
    board.context.strokeStyle = Board.SELECTION_COLOR;
  } else {
    board.context.lineWidth = Board.DEFAULT_LINE_WIDTH;
  }

  board.context.stroke();
  board.context.closePath();
  
  drawArrow();
}

void drawArrow() {
  //board.context.moveTo(output.center().x, output.center().y);
}

Map<String, Object> toJson() {
  var flow = new Map<String, Object>();
  flow["id"] = id;
  flow["name"] = name;
  flow["input"] = input.id;
  flow["output"] = output.id;

  return flow;
}

void select() {
  _selected = true;
  board.lastFlowSelected = this;
  board.toolBar.bringSelectedFlow();
}

void deselect() {
  _selected = false;
  board.lastFlowSelected = null;
}

void toggleSelection(){
  if (isSelected()) {
    deselect();
  } else {
    select();
  }
}

bool isSelected() => _selected;

}
    