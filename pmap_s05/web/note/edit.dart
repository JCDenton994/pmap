import 'dart:html';
import 'package:pmap/note.dart';
import 'package:polymer/polymer.dart';
import 'select.dart';

@CustomTag('note-edit')
class CategoryEdit extends PolymerElement {
  @published Notes notes;
  @published Note note;
  @published String user;
  @published String comment;

  CategoryEdit.created() : super.created();

  enteredView() {
    super.enteredView();
    user = note.user;
    comment = note.comment;
  }

  update(Event e, var detail, Node target) {
    note.user = user;
    note.comment = comment;
    notes.order();
    var polymerApp = querySelector('#note-component');
    CategoryTable noteTable =
        polymerApp.shadowRoot.querySelector('#note-select');
    noteTable.showEdit = false;
  }
}