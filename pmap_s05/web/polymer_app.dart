import 'dart:html';
import 'dart:convert';
import 'package:pmap/note.dart';
import 'package:polymer/polymer.dart';

@CustomTag('note-component')
class PolymerApp extends PolymerElement {
  static const String NAME = 'pmapnote';
  @observable Notes notes;

  PolymerApp.created() : super.created() {
    var noteModel = new NoteModel();
    notes = noteModel.notes;

    // load data
    String json = window.localStorage[NAME];
    if (json == null) {
      noteModel.init();
    } else {
      notes.fromJson(JSON.decode(json));
    }

    notes.internalList = toObservable(notes.internalList);
  }

  save(Event e, var detail, Node target) {
    window.localStorage[NAME] = JSON.encode(notes.toJson());
  }
}