part of pmap;

class Flow {

  final Board board;
  Shape input;
  Shape output;
  LineEquation line;
  String id; // RT: attribut non utilisé.
  String name; // RT: attribut non utilisé.
  
  bool _selected = false;

Flow (this.board, this.input, this.output) {
  //id = ' '; //add increment to this attribute.
  name = ' '; //will be updated by toolbar.
  draw();
  select();
}

void draw() {
  board.context..beginPath()
    ..moveTo(input.center().x, input.center().y)
    ..lineTo(output.center().x, output.center().y)
    ..strokeStyle = Board.SELECTION_COLOR;
  
  if (isSelected()) {
    board.context..lineWidth = Board.DEFAULT_LINE_WIDTH + 2
      ..strokeStyle = Board.SELECTION_COLOR;
  } else {
    board.context.lineWidth = Board.DEFAULT_LINE_WIDTH;
  }

  board.context..stroke()..closePath();
  drawArrow();
}

void drawArrow() {
  const DISTANCE = 10;
  num xOrientation, yOrientation, deltaY, deltaX, xEnd, yEnd;
  line = new LineEquation(input.center(), output.center());
  
  if (input.x > output.x) {
    xOrientation = -1;
  } else if (input.x < output.x) {
    xOrientation = 1;
  } else {
    xOrientation = 0;
  }
  
  if (input.y > output.y) {
    yOrientation = -1;
  } else if (input.x < output.x) {
    yOrientation = 1;
  } else {
    yOrientation = 0;
  }
  
  deltaX = sqrt(pow(output.x - input.x, 2));
  deltaY = sqrt(pow(output.y - input.y, 2));
  
  if (deltaY >= deltaX) {
    yEnd = output.center().y + ((output.height / 2 + DISTANCE) * -yOrientation);
    xEnd = line.getX(yEnd);
  } else {
    xEnd = output.center().x + ((output.width / 2 + DISTANCE) * -xOrientation);
    yEnd = line.getY(xEnd);
  }
  
  board.context..beginPath()
    ..moveTo(xEnd, yEnd)
      ..arc(xEnd, yEnd, 5, 0, PI * 2, true)
      ..fillStyle = 'black'
      ..fill()
      ..stroke()
      ..closePath();
}

Map<String, Object> toJson() {
  var flow = new Map<String, Object>();
  flow["id"] = id;
  flow["name"] = name;
  flow["input"] = input.id;
  flow["output"] = output.id;

  return flow;
}

void select() {
  _selected = true;
  board.lastFlowSelected = this;
  board.toolBar.bringSelectedFlow();
}

void deselect() {
  _selected = false;
  board.lastFlowSelected = null;
}

void toggleSelection(){
  if (isSelected()) {
    deselect();
  } else {
    select();
  }
}

bool isSelected() => _selected;

}
    