part of pmap;

class Board {
  static const int MIN_WIDTH = 800;
  static const int MIN_HEIGHT = 600;
  static const int DEFAULT_LINE_WIDTH = 1;
  static const int DEFAULT_FONT_SIZE = 12;
  static const String DEFAULT_LINE_COLOR = '#FFFFFF';
  static const String SOFT_LINE_COLOR = '#999493';
  static const String SELECTION_COLOR = '#000000';
  static const int SELECTION_WIDTH = 1;

  static const int DELTA = 8;

  static const String FILE_NAME = 'process.txt';

  CanvasElement canvas;
  CanvasRenderingContext2D context;

  num _width;
  num _height;
  num count = 0;
  int x = 0;
  int y = 0;
  
  String name = '';
  
  List<Shape> shapes;
  List<Flow> flows;

  Shape beforeLastShapeSelected;
  Shape lastShapeSelected;
  Flow lastFlowSelected;

  MenuBar menuBar;
  ToolBar toolBar;

  Board(this.canvas) {
    context = canvas.getContext('2d');
    _width = canvas.width;
    _height = canvas.height;

    shapes = new List<Shape>();
    flows = new List<Flow>();
    menuBar = new MenuBar(this);
    toolBar = new ToolBar(this);
    
    document.querySelector('#canvas').onMouseDown.listen(onMouseDown);
    window.animationFrame.then(redrawLoop);
  }

  void redrawLoop(num delta) {
    redraw();
    window.animationFrame.then(redrawLoop);
  }
  
  void set width(num width) {
    _width = width;
    canvas.width = width.toInt();
  }

  num get width {
    return _width;
  }

  void set height(num height) {
    _height = height;
    canvas.height = height.toInt();
  }

  num get height {
    return _height;
  }
  
  void clear() {
    context.clearRect(0, 0, width, height);
    border();
  }
  
  void redraw() {
    for(Shape shape in shapes) {
      num x = shape.x;
      num y = shape.y;
      name = shape.name; // RT: il me semble y avoir quelque chose qui cloche.
      String type = shape.type;
      shape.draw(x, y, type);
    }
    
    /*for (Flow flow in flows) {
      flow.draw();
    }*/
  }
  
  
  void openModel(String name) {
    String json = window.localStorage[name];
    if (json != null) {
      fromJson(json);
    }
  }

  void saveModel(String name) {
    String json = toJson();
    if (json != null) {
      window.localStorage[name] = json;
    }
  }

  void closeModel() {
    delete();
    clear();
  }
  
  String toJson() {
    var boardMap = new Map<String, Object>();
    boardMap["width"] = width;
    boardMap["height"] = height;
    boardMap["shapes"] = shapesToJson();
    boardMap["flows"] = flowsToJson();
    return JSON.encode(boardMap);
  }
  
  void fromJson(String json) {
    if (json != null && json.trim() != '') {
      Map<String, Object> boardMap = JSON.decode(json);
      width = boardMap["width"];
      height = boardMap["height"];
      List<Map<String, Object>> shapesList = boardMap["shapes"];
      List<Map<String, Object>> flowList = boardMap["flows"];
      shapesFromJson(shapesList);
      flowsFromJson(flowList);
    }
  }
  
  List<Map<String, Object>> shapesToJson() {
    List<Map<String, Object>> shapesList = new List<Map<String, Object>>();
    for (Shape shape in shapes) {
      shapesList.add(shape.toJson());
    }
    return shapesList;
  }
  
  List<Map<String, Object>> flowsToJson() {
    List<Map<String, Object>> flowList = new List<Map<String, Object>>();
    for (Flow flow in flows) {
      flowList.add(flow.toJson());
    }
    return flowList;
  }
    
  void shapesFromJson(List<Map<String, Object>> shapesList) {
    shapes = new List<Shape>();
    for (Map<String, Object> jsonShape in shapesList) {
      shapes.add(shapeFromJson(jsonShape));
    }
  }
  
  Shape shapeFromJson(Map<String, Object> shapeMap) {
    num idshape = shapeMap["id"];
    num x = shapeMap["x"];
    num y = shapeMap["y"];
    String name = shapeMap["name"];
    String type = shapeMap["type"];
    num width = shapeMap["width"];
    num height = shapeMap["height"];
    var shape = new Shape(this, idshape, name, x, y, type);
    shape.name = shapeMap["name"];
    shape.height = height;
    shape.width = width;
    List<Map<String, Object>> itemsList = shapeMap["items"];
    return shape;
  }
  
  void flowsFromJson(List<Map<String, Object>> flowList) {
    flows = new List<Flow>();
    for (Map<String, Object> jsonFlow in flowList) {
      flows.add(flowFromJson(jsonFlow));
    }
  }
  
  Flow flowFromJson(Map<String, Object> flowMap) {
    Flow flow = null;
    num id = flowMap["id"];
    String name = flowMap["name"];
    num inputId = flowMap["input"];
    num outputId = flowMap["output"];
    Shape input = findShapeById(inputId);
    Shape output = findShapeById(outputId);
    if (input != null && output != null) {
      flow = new Flow(this, input, output);
    }
    return flow;
  }
  
  void border() {
    context..beginPath()
      ..rect(0, 0, width, height)
      ..lineWidth = DEFAULT_LINE_WIDTH
      ..strokeStyle = DEFAULT_LINE_COLOR
      ..stroke()
      ..closePath();
  }

   
  void onMouseDown(MouseEvent e) {
    bool clickedOnShape = false;
    for (Shape shape in shapes) {
      if (shape.contains(e.offset.x, e.offset.y)){
        clickedOnShape = true;
        break;
      }
    }
    x = e.offset.x;
    y = e.offset.y;
    if (toolBar.isSelectToolOn()) {
      Point clickedPoint = new Point(e.offset.x, e.offset.y);
    } else if (toolBar.isdata_objectToolOn()) {
      count = ++count;
      var shape = new Shape(this, count, name, x, y, "data_object");
      shapes.add(shape);   
    } else if (toolBar.isdata_storeToolOn()) {
      count = ++count;
      var shape = new Shape(this, count, name, x, y, "data_store");
      shapes.add(shape);
    } else if (toolBar.isendToolOn()) {
      count = ++count;
      var shape = new Shape(this, count, name, x, y, "end");
      shapes.add(shape);
    } else if (toolBar.iseventToolOn()) {
      count = ++count;
      var shape = new Shape(this, count, name, x, y, "event");
      shapes.add(shape);
    } else if (toolBar.isgatewayToolOn()) {
      count = ++count;      
      var shape = new Shape(this, count, name, x, y, "gateway");
      shapes.add(shape);
    } else if (toolBar.ismessageToolOn()) {
      count = ++count;
      var shape = new Shape(this, count, name, x, y, "message");
      shapes.add(shape);
    } else if (toolBar.isstartToolOn()) {
      count = ++count;
      var shape = new Shape(this, count, name, x, y, "start");
      shapes.add(shape);
    } else if (toolBar.issubToolOn()) {
      count = ++count;
      var shape = new Shape(this, count, name, x, y, "sub");
      shapes.add(shape);
    } else if (toolBar.istaskToolOn()) {
      count = ++count;
      var shape = new Shape(this, count, name, x, y, "task");
      shapes.add(shape);
    } else if (toolBar.isflowToolOn()) {
      // Create a flow between the last two selected shapes.
      // RT: bogue de sélection: beforeLastShapeSelected et lastShapeSelected à null.
      /*if (beforeLastShapeSelected != null && lastShapeSelected != null &&
        _shapeExists(beforeLastShapeSelected) && _shapeExists(lastShapeSelected) &&
        countLinesBetween(beforeLastShapeSelected, lastShapeSelected) < 2) {
        var flow = new Flow(this, beforeLastShapeSelected, lastShapeSelected);
        flows.add(flow);
      }*/
      // RT: code de test, ligne entre chaque shapes, en ordre de création.
      flows.clear();
      for (int i = 1; i < shapes.length; i++) {
        var flow = new Flow(this, shapes[i - 1], shapes[i]);
        flows.add(flow);
      }
    }
    
    toolBar.backToFixedTool();
  }
  
  void delete() {
    shapes.clear();
    flows.clear();
  }
  
  int countShapesContain(int pointX, int pointY) {
    int count = 0;
    for (Shape shape in shapes) {
      if (shape.contains(pointX, pointY)) {
        count++;
      }
    }
    return count;
  }
  
  Shape findShape(String shapeName) {
    for (Shape shape in shapes) {
      if (shape.name == shapeName) {
        return shape;
      }
    }
    return null;
  }
  
  Shape findShapeById (num id) {
    for (Shape shape in shapes) {
      if (shape.id == id) {
        return shape;
      }
    }
    return null;
  }
  
  /*Flow getFlowStartingFromShape(Shape shape) {
    for (Flow flow in flows) {
      if (flow.input == shape) {
        return flow;
      }
    }
    return null;
  }*/
  
  bool _shapeExists(Shape shape) {
    for (Shape s in shapes) {
      if (s == shape) {
        return true;
      }
    }
    return false;
  }
  
}