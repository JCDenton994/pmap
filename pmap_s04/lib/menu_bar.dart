part of pmap;

class MenuBar {

  final Board board;
  
  InputElement modelNameInput;
  ButtonElement openProcessButton;
  ButtonElement saveProcessButton;
  ButtonElement closeProcessButton;

  MenuBar(this.board) {
    modelNameInput = document.querySelector('#process-name');
    openProcessButton = document.querySelector('#open-process');
    saveProcessButton = document.querySelector('#save-process');
    closeProcessButton = document.querySelector('#close-process');

    openProcessButton.onClick.listen((MouseEvent e) {
      String modelName = modelNameInput.value.trim();
      board.openModel(modelName);
    });
    saveProcessButton.onClick.listen((MouseEvent e) {
      String modelName = modelNameInput.value.trim();
      board.saveModel(modelName);
    });
    closeProcessButton.onClick.listen((MouseEvent e) {
      modelNameInput.value = '';
      board.closeModel();
    });
  }


}
