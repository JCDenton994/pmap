part of pmap;

class Shape {
  final Board board;
  String name;
  num idshape;
  num x;
  num y;
  String type;
  
  
  bool data_object = false;
  bool data_store = false;
  bool end = false;
  bool event = false;
  bool gateway = false;
  bool message = false;
  bool start = false;
  bool sub = false;
  bool task = false;
  bool entry = false;
  bool _selected = false;
  bool _hidden = false;
  bool _mouseDown = false;
  
bool contains(int pointX, int pointY) {
  if((pointX > x) && (pointY > y)) {
      return true;
  } else {
      return false;
  }
}
Shape(this.board, this.idshape, this.name, this.x, this.y, this.type) {
  draw(this.x, this.y, this.type);
  document.querySelector('#canvas').onMouseDown.listen(onMouseDown);
  document.querySelector('#canvas').onMouseUp.listen(onMouseUp);  
  document.querySelector('#canvas').onMouseMove.listen(onMouseMove);
  select();
}

void draw(x, y, type) {
    ImageElement image = new ImageElement(src: "img/"+type+".png");
    image.onLoad.listen((e) {
      board.context.drawImage(image, x, y);
    });
}

void select() {
  _selected = true;
  if (board.lastShapeSelected != this) {
    board.beforeLastShapeSelected = board.lastShapeSelected;
  }
  board.lastShapeSelected = this;
  board.toolBar.bringSelectedShape();
}

void deselect() {
  _selected = false;
  if (board.lastShapeSelected == this){
    board.lastShapeSelected = board.beforeLastShapeSelected;
    board.beforeLastShapeSelected = null;
  } else if (board.beforeLastShapeSelected == this) {
    board.beforeLastShapeSelected = null;
  }
}

bool isSelected() => _selected;

void toggleSelection() {
  if (isSelected()) {
    deselect();
  } else {
    select();
  }
}

Map<String, Object> toJson(){
  Map<String, Object> shapeMap = new Map<String, Object>();
  shapeMap["idshape"] = idshape;
  shapeMap["name"] = name;
  shapeMap["x"] = x;
  shapeMap["y"] = y;
  shapeMap["type"] = type;
  return shapeMap;
}

String toString() => '$idshape, $name, $type ($x, $y)';

void onMouseDown(MouseEvent e) {
  _mouseDown = true;
  if (board.toolBar.isSelectToolOn() && contains(e.offset.x, e.offset.y)) {
    toggleSelection();
  }
}

void onMouseUp(MouseEvent e) {
  _mouseDown = false;
}


void onMouseMove(MouseEvent e) {
  if (contains(e.offset.x, e.offset.y) && _mouseDown &&
    board.countShapesContain(e.offset.x, e.offset.y) < 2) {
     x = (e.offset.x / 2).toInt();
     if (x < 0) {
    x = 1;
     }
     if (x > board.width) {
    x = board.width - 1;
     }
     y = (e.offset.y / 2).toInt();
     if (y < 0) {
    y = 1;
     }
     if (y > board.height) {
    y = board.height - 1;
     }
  }   
}

}