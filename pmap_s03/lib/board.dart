part of pmap;

class Board {
  static const int MIN_WIDTH = 990;
  static const int MIN_HEIGHT = 580;
  static const int DEFAULT_LINE_WIDTH = 1;
  static const int DEFAULT_FONT_SIZE = 12;
  static const String DEFAULT_LINE_COLOR = '#000000';
  static const String SOFT_LINE_COLOR = '#999493';
  static const String SELECTION_COLOR = '#000000';

  static const int DELTA = 8;

  static const String FILE_NAME = 'process.txt';

  CanvasElement canvas;
  CanvasRenderingContext2D context;

  num _width;
  num _height;
  num count = 0;
  int X = 0;
  int Y = 0;

  List<Shape> shapes;
  List<Line> lines;

  Shape beforeLastShapeSelected;
  Shape lastShapeSelected;
  Line lastLineSelected;

  MenuBar menuBar;
  ToolBar toolBar;

  Board(this.canvas) {
    context = canvas.getContext('2d');
    _width = canvas.width;
    _height = canvas.height;

    shapes = new List<Shape>();
    lines = new List<Line>();
    menuBar = new MenuBar(this);
    toolBar = new ToolBar(this);
    
    document.querySelector('#canvas').onMouseDown.listen(onMouseDown);
    window.animationFrame.then(redrawLoop);
  }

  void redrawLoop(num delta) {
    redraw();
    window.animationFrame.then(redrawLoop);
  }
  
  void set width(num width) {
    _width = width;
    canvas.width = width.toInt();
  }

  num get width {
    return _width;
  }

  void set height(num height) {
    _height = height;
    canvas.height = height.toInt();
  }

  num get height {
    return _height;
  }
  
  void clear() {
    context.clearRect(0, 0, width, height);
    border();
  }
  
  void redraw(){
      for(Shape shape in shapes){
      num x = shape.x;
      num y = shape.y;
      String type = shape.type;
      shape.draw(x, y, type);
    }
  }
  
  
  void openModel(String name) {
    String json = window.localStorage[name];
    if (json != null) {
      fromJson(json);
    }
  }

  void saveModel(String name) {
    String json = toJson();
    if (json != null) {
      window.localStorage[name] = json;
    }
  }

  void closeModel() {
    delete();
  }
  
  String toJson() {
    Map<String, Object> boardMap = new Map<String, Object>();
    boardMap["width"] = width;
    boardMap["height"] = height;
    boardMap["shapes"] = shapesToJson();
    return JSON.encode(boardMap);
  }
  
  void fromJson(String json) {
    if (json != null && json.trim() != '') {
      Map<String, Object> boardMap = JSON.decode(json);
      width = boardMap["width"];
      height = boardMap["height"];
      List<Map<String, Object>> shapesList = boardMap["shapes"];
      shapesFromJson(shapesList);
    }
  }
  
  List<Map<String, Object>> shapesToJson() {
    List<Map<String, Object>> shapesList = new List<Map<String, Object>>();
    for (Shape shape in shapes) {
        shapesList.add(shape.toJson());
    }
    return shapesList;
  }
    
  void shapesFromJson(List<Map<String, Object>> shapesList) {
    shapes = new List<Shape>();
    for (Map<String, Object> jsonShape in shapesList) {
      shapes.add(shapeFromJson(jsonShape));
    }
  }
  
  Shape shapeFromJson(Map<String, Object> shapeMap) {
    num idshape = shapeMap["idshape"];
    num x = shapeMap["x"];
    num y = shapeMap["y"];
    String type = shapeMap["type"];
    num width = shapeMap["width"];
    num height = shapeMap["height"];
    Shape shape = new Shape(this, idshape, x, y, type);
    shape.name = shapeMap["name"];
    List<Map<String, Object>> itemsList = shapeMap["items"];
    return shape;
  }
  
  void border() {
    context.beginPath();
    context.rect(0, 0, width, height);
    context.lineWidth = DEFAULT_LINE_WIDTH;
    context.strokeStyle = DEFAULT_LINE_COLOR;
    context.stroke();
    context.closePath();
  }

   
  void onMouseDown(MouseEvent e) {
    X = e.offset.x;
    Y = e.offset.y;
      if (toolBar.isSelectToolOn()) {
        Point clickedPoint = new Point(e.offset.x, e.offset.y);
      } else if (toolBar.isdata_objectToolOn()) {
        count = ++count;
        Shape shape = new Shape(this, count, X, Y, "data_object");
        shapes.add(shape);
        
      } else if (toolBar.isdata_storeToolOn()) {
        count = ++count;
        Shape shape = new Shape(this, count, X, Y, "data_store");
        shapes.add(shape);
        
      } else if (toolBar.isendToolOn()) {
        count = ++count;
        Shape shape = new Shape(this, count, X, Y, "end");
        shapes.add(shape);
        
      } else if (toolBar.iseventToolOn()) {
        count = ++count;
        Shape shape = new Shape(this, count, X, Y, "event");
        shapes.add(shape);
        
      } else if (toolBar.isgatewayToolOn()) {
        count = ++count;      
        Shape shape = new Shape(this, count, X, Y, "gateway");
        shapes.add(shape);
        
      } else if (toolBar.ismessageToolOn()) {
        count = ++count;
        Shape shape = new Shape(this, count, X, Y, "message");
        shapes.add(shape);
        
      } else if (toolBar.isstartToolOn()) {
        count = ++count;
        Shape shape = new Shape(this, count, X, Y, "start");
        shapes.add(shape);
        
      } else if (toolBar.issubToolOn()) {
        count = ++count;
        Shape shape = new Shape(this, count, X, Y, "sub");
        shapes.add(shape);
        
      } else if (toolBar.istaskToolOn()) {
        count = ++count;
        Shape shape = new Shape(this, count, X, Y, "task");
        shapes.add(shape);
      }
      
      toolBar.backToFixedTool();
    }
  
  void delete() {
    shapes.clear();
  }
  
  int countShapesContain(int pointX, int pointY) {
    int count = 0;
    for (Shape shape in shapes) {
      if (shape.contains(pointX, pointY)) {
        count++;
      }
    }
    return count;
  }
  
  Shape findShape(String shapeName) {
    for (Shape shape in shapes) {
      if (shape.name == shapeName) {
        return shape;
      }
    }
    return null;
  }
}