part of pmap;

class ToolBar {
  static const int SELECT = 1;
  static const int DATA_OBJECT = 2;
  static const int DATA_STORE = 3;
  static const int END = 4;
  static const int EVENT = 5;
  static const int GATEWAY = 6;
  static const int MESSAGE = 7;
  static const int START = 8;
  static const int SUB = 9;
  static const int TASK = 10;
  static const int LINE = 11;

  final Board board;
  
  int _onTool;
  int _fixedTool;

  ButtonElement selectButton;
  ButtonElement lineButton; 
  ButtonElement data_objectButton;
  ButtonElement data_storeButton;
  ButtonElement endButton;
  ButtonElement eventButton;
  ButtonElement gatewayButton;
  ButtonElement messageButton;
  ButtonElement startButton;
  ButtonElement subButton;
  ButtonElement taskButton; 
  
  InputElement shapeNameInput;
  InputElement canvasWidthInput;
  InputElement canvasHeightInput;

  SelectElement lineSelect;
  
ToolBar(this.board) {
  selectButton = document.querySelector('#select');
  data_objectButton = document.querySelector('#data_object');
  data_storeButton = document.querySelector('#data_store');
  endButton = document.querySelector('#end');
  eventButton = document.querySelector('#event');
  gatewayButton = document.querySelector('#gateway');
  messageButton = document.querySelector('#message');
  startButton = document.querySelector('#start');
  subButton = document.querySelector('#sub');
  taskButton = document.querySelector('#task');
  lineButton = document.querySelector('#line');
  
  selectButton.onClick.listen((MouseEvent e) {
    onTool(SELECT);
  });
  selectButton.onDoubleClick.listen((MouseEvent e) {
    onTool(SELECT);
    _fixedTool = SELECT;
  });
  
  data_objectButton.onClick.listen((MouseEvent e) {
    onTool(DATA_OBJECT);
  });
  data_objectButton.onDoubleClick.listen((MouseEvent e) {
    onTool(DATA_OBJECT);
    _fixedTool = DATA_OBJECT;
  });
  
  data_storeButton.onClick.listen((MouseEvent e) {
    onTool(DATA_STORE);
  });
  data_storeButton.onDoubleClick.listen((MouseEvent e) {
    onTool(DATA_STORE);
    _fixedTool = DATA_STORE;
  });
  
  endButton.onClick.listen((MouseEvent e) {
    onTool(END);
  });
  endButton.onDoubleClick.listen((MouseEvent e) {
    onTool(END);
    _fixedTool = END;
  });
  
  eventButton.onClick.listen((MouseEvent e) {
    onTool(EVENT);
  });
  eventButton.onDoubleClick.listen((MouseEvent e) {
    onTool(EVENT);
    _fixedTool = EVENT;
  });
  
  gatewayButton.onClick.listen((MouseEvent e) {
    onTool(GATEWAY);
  });
  gatewayButton.onDoubleClick.listen((MouseEvent e) {
    onTool(GATEWAY);
    _fixedTool = GATEWAY;
  });
  
  messageButton.onClick.listen((MouseEvent e) {
    onTool(MESSAGE);
  });
  messageButton.onDoubleClick.listen((MouseEvent e) {
    onTool(MESSAGE);
    _fixedTool = MESSAGE;
  });
  
  startButton.onClick.listen((MouseEvent e) {
    onTool(START);
  });
  startButton.onDoubleClick.listen((MouseEvent e) {
    onTool(START);
    _fixedTool = START;
  });
  
  subButton.onClick.listen((MouseEvent e) {
    onTool(SUB);
  });
  subButton.onDoubleClick.listen((MouseEvent e) {
    onTool(SUB);
    _fixedTool = SUB;
  });
  
  taskButton.onClick.listen((MouseEvent e) {
    onTool(TASK);
  });
  taskButton.onDoubleClick.listen((MouseEvent e) {
    onTool(TASK);
    _fixedTool = TASK;
  });
  
  selectButton.onClick.listen((MouseEvent e) {
    onTool(LINE);
  });
  selectButton.onDoubleClick.listen((MouseEvent e) {
    onTool(LINE);
    _fixedTool = LINE;
  });

  onTool(SELECT);
  _fixedTool = SELECT;

  canvasWidthInput = document.querySelector('#canvasWidth');
  canvasHeightInput = document.querySelector('#canvasHeight');
  canvasWidthInput.valueAsNumber = board.width;
  canvasWidthInput.onChange.listen((Event e) {
    board.width = canvasWidthInput.valueAsNumber;
  });
  canvasHeightInput.valueAsNumber = board.height;
  canvasHeightInput.onChange.listen((Event e) {
    board.height = canvasHeightInput.valueAsNumber;
  });
  
  shapeNameInput = document.querySelector('#shapeName');
  shapeNameInput.onChange.listen((Event e) {
    Shape shape = board.lastShapeSelected;
    if (shape != null) {
      String shapeName = shapeNameInput.value.trim();
      if (shapeName != '') {
        Shape otherShape = board.findShape(shapeName);
        if (otherShape == null) {
    shape.name = shapeName;
        }
            }
          }
  });
}

void bringSelectedShape() {
  Shape shape = board.lastShapeSelected;
  if (shape != null) {
    shapeNameInput.value = shape.name;
  }
}

onTool(int tool) {
  _onTool = tool;
  if (_onTool == SELECT) {
    selectButton.style.borderColor = Board.DEFAULT_LINE_COLOR;
    lineButton.style.borderColor = Board.SOFT_LINE_COLOR;
    data_objectButton.style.borderColor = Board.SOFT_LINE_COLOR;
    data_storeButton.style.borderColor = Board.SOFT_LINE_COLOR;
    endButton.style.borderColor = Board.SOFT_LINE_COLOR;
    eventButton.style.borderColor = Board.SOFT_LINE_COLOR;
    gatewayButton.style.borderColor = Board.SOFT_LINE_COLOR;
    messageButton.style.borderColor = Board.SOFT_LINE_COLOR;
    startButton.style.borderColor = Board.SOFT_LINE_COLOR;
    subButton.style.borderColor = Board.SOFT_LINE_COLOR;
    taskButton.style.borderColor = Board.SOFT_LINE_COLOR;
    
  } else if (_onTool == DATA_OBJECT) {
    selectButton.style.borderColor = Board.SOFT_LINE_COLOR;
    lineButton.style.borderColor = Board.SOFT_LINE_COLOR;
    data_objectButton.style.borderColor = Board.DEFAULT_LINE_COLOR;
    data_storeButton.style.borderColor = Board.SOFT_LINE_COLOR;
    endButton.style.borderColor = Board.SOFT_LINE_COLOR;
    eventButton.style.borderColor = Board.SOFT_LINE_COLOR;
    gatewayButton.style.borderColor = Board.SOFT_LINE_COLOR;
    messageButton.style.borderColor = Board.SOFT_LINE_COLOR;
    startButton.style.borderColor = Board.SOFT_LINE_COLOR;
    subButton.style.borderColor = Board.SOFT_LINE_COLOR;
    taskButton.style.borderColor = Board.SOFT_LINE_COLOR;
 
  } else if (_onTool == DATA_STORE) {
    selectButton.style.borderColor = Board.SOFT_LINE_COLOR;
    lineButton.style.borderColor = Board.SOFT_LINE_COLOR;
    data_objectButton.style.borderColor = Board.SOFT_LINE_COLOR;
    data_storeButton.style.borderColor = Board.DEFAULT_LINE_COLOR;
    endButton.style.borderColor = Board.SOFT_LINE_COLOR;
    eventButton.style.borderColor = Board.SOFT_LINE_COLOR;
    gatewayButton.style.borderColor = Board.SOFT_LINE_COLOR;
    messageButton.style.borderColor = Board.SOFT_LINE_COLOR;
    startButton.style.borderColor = Board.SOFT_LINE_COLOR;
    subButton.style.borderColor = Board.SOFT_LINE_COLOR;
    taskButton.style.borderColor = Board.SOFT_LINE_COLOR;
    
  } else if (_onTool == END) {
    selectButton.style.borderColor = Board.SOFT_LINE_COLOR;
    lineButton.style.borderColor = Board.SOFT_LINE_COLOR;
    data_objectButton.style.borderColor = Board.SOFT_LINE_COLOR;
    data_storeButton.style.borderColor = Board.SOFT_LINE_COLOR;
    endButton.style.borderColor = Board.DEFAULT_LINE_COLOR;
    eventButton.style.borderColor = Board.SOFT_LINE_COLOR;
    gatewayButton.style.borderColor = Board.SOFT_LINE_COLOR;
    messageButton.style.borderColor = Board.SOFT_LINE_COLOR;
    startButton.style.borderColor = Board.SOFT_LINE_COLOR;
    subButton.style.borderColor = Board.SOFT_LINE_COLOR;
    taskButton.style.borderColor = Board.SOFT_LINE_COLOR;
    
  } else if (_onTool == EVENT) {
    selectButton.style.borderColor = Board.SOFT_LINE_COLOR;
    lineButton.style.borderColor = Board.SOFT_LINE_COLOR;
    data_objectButton.style.borderColor = Board.SOFT_LINE_COLOR;
    data_storeButton.style.borderColor = Board.SOFT_LINE_COLOR;
    endButton.style.borderColor = Board.SOFT_LINE_COLOR;
    eventButton.style.borderColor = Board.DEFAULT_LINE_COLOR;
    gatewayButton.style.borderColor = Board.SOFT_LINE_COLOR;
    messageButton.style.borderColor = Board.SOFT_LINE_COLOR;
    startButton.style.borderColor = Board.SOFT_LINE_COLOR;
    subButton.style.borderColor = Board.SOFT_LINE_COLOR;
    taskButton.style.borderColor = Board.SOFT_LINE_COLOR;
    
  } else if (_onTool == GATEWAY) {
    selectButton.style.borderColor = Board.SOFT_LINE_COLOR;
    lineButton.style.borderColor = Board.SOFT_LINE_COLOR;
    data_objectButton.style.borderColor = Board.SOFT_LINE_COLOR;
    data_storeButton.style.borderColor = Board.SOFT_LINE_COLOR;
    endButton.style.borderColor = Board.SOFT_LINE_COLOR;
    eventButton.style.borderColor = Board.SOFT_LINE_COLOR;
    gatewayButton.style.borderColor = Board.DEFAULT_LINE_COLOR;
    messageButton.style.borderColor = Board.SOFT_LINE_COLOR;
    startButton.style.borderColor = Board.SOFT_LINE_COLOR;
    subButton.style.borderColor = Board.SOFT_LINE_COLOR;
    taskButton.style.borderColor = Board.SOFT_LINE_COLOR;
    
  } else if (_onTool == MESSAGE) {
    selectButton.style.borderColor = Board.SOFT_LINE_COLOR;
    lineButton.style.borderColor = Board.SOFT_LINE_COLOR;
    data_objectButton.style.borderColor = Board.SOFT_LINE_COLOR;
    data_storeButton.style.borderColor = Board.SOFT_LINE_COLOR;
    endButton.style.borderColor = Board.SOFT_LINE_COLOR;
    eventButton.style.borderColor = Board.SOFT_LINE_COLOR;
    gatewayButton.style.borderColor = Board.SOFT_LINE_COLOR;
    messageButton.style.borderColor = Board.DEFAULT_LINE_COLOR;
    startButton.style.borderColor = Board.SOFT_LINE_COLOR;
    subButton.style.borderColor = Board.SOFT_LINE_COLOR;
    taskButton.style.borderColor = Board.SOFT_LINE_COLOR;
    
  } else if (_onTool == START) {
    selectButton.style.borderColor = Board.SOFT_LINE_COLOR;
    lineButton.style.borderColor = Board.SOFT_LINE_COLOR;
    data_objectButton.style.borderColor = Board.SOFT_LINE_COLOR;
    data_storeButton.style.borderColor = Board.SOFT_LINE_COLOR;
    endButton.style.borderColor = Board.SOFT_LINE_COLOR;
    eventButton.style.borderColor = Board.SOFT_LINE_COLOR;
    gatewayButton.style.borderColor = Board.SOFT_LINE_COLOR;
    messageButton.style.borderColor = Board.SOFT_LINE_COLOR;
    startButton.style.borderColor = Board.DEFAULT_LINE_COLOR;
    subButton.style.borderColor = Board.SOFT_LINE_COLOR;
    taskButton.style.borderColor = Board.SOFT_LINE_COLOR;
    
  } else if (_onTool == SUB) {
    selectButton.style.borderColor = Board.SOFT_LINE_COLOR;
    lineButton.style.borderColor = Board.SOFT_LINE_COLOR;
    data_objectButton.style.borderColor = Board.SOFT_LINE_COLOR;
    data_storeButton.style.borderColor = Board.SOFT_LINE_COLOR;
    endButton.style.borderColor = Board.SOFT_LINE_COLOR;
    eventButton.style.borderColor = Board.SOFT_LINE_COLOR;
    gatewayButton.style.borderColor = Board.SOFT_LINE_COLOR;
    messageButton.style.borderColor = Board.SOFT_LINE_COLOR;
    startButton.style.borderColor = Board.SOFT_LINE_COLOR;
    subButton.style.borderColor = Board.DEFAULT_LINE_COLOR;
    taskButton.style.borderColor = Board.SOFT_LINE_COLOR;
    
  } else if (_onTool == TASK) {
    selectButton.style.borderColor = Board.SOFT_LINE_COLOR;
    lineButton.style.borderColor = Board.SOFT_LINE_COLOR;
    data_objectButton.style.borderColor = Board.SOFT_LINE_COLOR;
    data_storeButton.style.borderColor = Board.SOFT_LINE_COLOR;
    endButton.style.borderColor = Board.SOFT_LINE_COLOR;
    eventButton.style.borderColor = Board.SOFT_LINE_COLOR;
    gatewayButton.style.borderColor = Board.SOFT_LINE_COLOR;
    messageButton.style.borderColor = Board.SOFT_LINE_COLOR;
    startButton.style.borderColor = Board.SOFT_LINE_COLOR;
    subButton.style.borderColor = Board.SOFT_LINE_COLOR;
    taskButton.style.borderColor = Board.DEFAULT_LINE_COLOR;
    
  } else if (_onTool == LINE) {
    selectButton.style.borderColor = Board.SOFT_LINE_COLOR;
    lineButton.style.borderColor = Board.DEFAULT_LINE_COLOR;
    data_objectButton.style.borderColor = Board.SOFT_LINE_COLOR;
    data_storeButton.style.borderColor = Board.SOFT_LINE_COLOR;
    endButton.style.borderColor = Board.SOFT_LINE_COLOR;
    eventButton.style.borderColor = Board.SOFT_LINE_COLOR;
    gatewayButton.style.borderColor = Board.SOFT_LINE_COLOR;
    messageButton.style.borderColor = Board.SOFT_LINE_COLOR;
    startButton.style.borderColor = Board.SOFT_LINE_COLOR;
    subButton.style.borderColor = Board.SOFT_LINE_COLOR;
    taskButton.style.borderColor = Board.SOFT_LINE_COLOR;
  }
 }
  bool isSelectToolOn() {
    if (_onTool == SELECT) {
      return true;
    }
    return false;
  }
  bool isdata_objectToolOn() {
    if (_onTool == DATA_OBJECT) {
      return true;
    }
    return false;
  }
  
  bool isdata_storeToolOn() {
    if (_onTool == DATA_STORE) {
      return true;
    }
    return false;
  }

  bool isendToolOn() {
    if (_onTool == END) {
      return true;
    }
    return false;
  }
  
  bool iseventToolOn() {
    if (_onTool == EVENT) {
      return true;
    }
    return false;
  }
  
  bool isgatewayToolOn() {
    if (_onTool == GATEWAY) {
      return true;
    }
    return false;
  }
  
  bool islineToolOn() {
    if (_onTool == LINE) {
      return true;
    }
    return false;
  }
  
  bool ismessageToolOn() {
    if (_onTool == MESSAGE) {
      return true;
    }
    return false;
  }
  
  bool isstartToolOn() {
    if (_onTool == START) {
      return true;
    }
    return false;
  }
  
  bool issubToolOn() {
    if (_onTool == SUB) {
      return true;
    }
    return false;
  }
  
  bool istaskToolOn() {
    if (_onTool == TASK) {
      return true;
    }
    return false;
  }
  
  void backToFixedTool()  {
      onTool(_fixedTool);
  }
}