part of pmap;

class Line {

  final Board board;
  Shape shape1;
  Shape shape2;
  String idline;
  String name;

  bool internal = true;
  bool _selected = false;

Line (this.board, this.shape1, this.shape2) {
  idline = ' '; //add increment to this attribute
  name = ' '; //will be updated by toolbar
  draw();
  select();
}

void draw() {
  board.context.beginPath();
  board.context.moveTo(shape1.x, shape1.y);
  board.context.lineTo(shape2.x, shape2.y);
  board.context.stroke();
  board.context.closePath();
}


Map<String, Object> toJson() {
  Map<String, Object> lineMap = new Map<String, Object>();
  lineMap["idline"] = idline;
  lineMap["name"] = name;
  lineMap["shape1"] = shape1;
  lineMap["shape2"] = shape2;

  return lineMap;
}

void select() {
  _selected = true;
  board.lastLineSelected = this;
  board.toolBar.bringSelectedLine();
}

void deselect() {
  _selected = false;
  board.lastLineSelected = null;
}

void toggleSelection(){
  if (isSelected()) {
    deselect();
  } else {
    select();
  }
}

bool isSelected() => _selected;

String _dropEnd(String text, String end) {
  String withoutEnd = text;
  int endPosition = text.lastIndexOf(end);
  if (endPosition > 0) {
    withoutEnd = text.substring(0, endPosition);
  }
  return withoutEnd;
}

}
  
  